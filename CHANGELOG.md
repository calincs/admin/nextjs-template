# nextjs_template

## 0.1.1

### Patch Changes

- lincs-auth-api:
  - simplify the request object by supressing the api version [6d5c55a4f8a52046cf8cc73acf0a164e99104c04]
- update dependencies [07081785b4d8fcd501b6b693971564624995fd9e]
  - core:
    - update: @ts-rest/core@3.27.0
    - bump:
      - @lincs.project/auth-api-contract@1.0.2
      - next@13.4.13
      - next-auth@4.22.5
  - dev:
    - upgrade eslint-config-prettier@9.0.0
    - update:
      - @typescript-eslint/eslint-plugin@6.3.0
      - @typescript-eslint/parser@6.3.0
    - bump:
      - @types/node@20.4.9
      - @types/react@18.2.19
      - eslint-config-next@13.4.13
