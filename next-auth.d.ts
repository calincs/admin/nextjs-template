import type { DefaultSession } from 'next-auth';
import type { DefaultJWT } from 'next-auth/jwt';
import type { AccountManagement, LinkedAccount } from './app/api/lincs/types';

// Read more at: https://next-auth.js.org/getting-started/typescript#module-augmentation
declare module 'next-auth/jwt' {
  interface JWT extends DefaultJWT {
    accountMangement?: AccountManagement;
    access_token?: string;
    accessTokenExpires?: number;
    error?: string;
    identityProvider?: string;
    linkedAccounts?: LinkedAccount[];
    provider?: string;
    refresh_token?: string;
    username?: string;
  }
}

declare module 'next-auth' {
  interface Profile {
    identity_provider: string;
    preferred_username?: string;
  }

  interface Session {
    access_token?: string;
    accessTokenExpires?: number;
    error?: string;
    provider?: string;
    user: {
      accountManagement?: AccountManagement;
      identityProvider?: string;
      linkedAccounts?: LinkedAccount[];
      username?: string;
    } & DefaultSession['user'];
  }
}
