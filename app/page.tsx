import styles from './page.module.css';
import { Profile } from './profile';
import { NextAuthProvider } from './providers';

export default async function Home() {
  return (
    <NextAuthProvider>
      <main className={styles.main}>
        <div className={styles.header}>
          <h1 className={styles.title}>Authentication Template</h1>
        </div>
        <div className={styles.center}>
          <Profile />
        </div>
        <footer className={styles.footer}>
          <p>
            Get started by editing&nbsp;
            <code className={styles.code}>app/page.tsx</code>
          </p>
          <a href="https://nextjs.org/" target="_blank" rel="noopener noreferrer">
            Powered by NEXT.js
          </a>
        </footer>
      </main>
    </NextAuthProvider>
  );
}
