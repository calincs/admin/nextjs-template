import { add, fromUnixTime, isBefore } from 'date-fns';
import queryString from 'query-string';
import type { AccountManagement, IdentityProviderToken, LinkedAccount } from '../types';
import { lincsAuthApi } from './auth-api';

interface RefreshAccessTokenResponse {
  access_token?: string;
  accessTokenExpires?: number;
  refresh_token?: string;
  error?: string;
}

interface RefreshAccessTokenResponseFromKC {
  access_token: string;
  expires_in: number;
  id_token: string;
  'not-before-policy': number;
  refresh_expires_in: number;
  refresh_token: string;
  scope: string;
  session_state: string;
  token_type: 'Bearer';
}

// -------------

const TOKEN_EXPIRES_IN = 4; // minutes

/**
 * The function checks if an access token has expired based on its expiration time.
 * @param [accessTokenExpires=0] - The `accessTokenExpires` parameter represents the expiration time of
 * an access token. It is a Unix timestamp that indicates when the access token will expire.
 * @returns a boolean value indicating whether the access token has expired or not.
 */
export const isAccessTokenExpired = async (accessTokenExpires = 0) => {
  const willExpiredIn = add(new Date(), { minutes: TOKEN_EXPIRES_IN });
  const isExpired = isBefore(fromUnixTime(accessTokenExpires), willExpiredIn);
  return isExpired;
};

/**
 * The function `refreshAccessToken` is an asynchronous function that takes a refresh token as input
 * and returns a promise that resolves to a response object containing refreshed access tokens.
 * @param {string | undefined} refresh_token - The `refresh_token` parameter is a string that
 * represents the refresh token used to obtain a new access token. It is passed as an argument to the
 * `refreshAccessToken` function.
 * @returns The function `refreshAccessToken` returns a Promise that resolves to a
 * `RefreshAccessTokenResponse` object.
 */
export const refreshAccessToken = async (
  refresh_token: string | undefined,
): Promise<RefreshAccessTokenResponse> => {
  try {
    const params = {
      grant_type: 'refresh_token',
      client_id: process.env.KEYCLOAK_ID,
      client_secret: process.env.KEYCLOAK_SECRET,
      refresh_token: refresh_token ?? '',
    };

    const options = {
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      method: 'POST',
      body: new URLSearchParams(params),
    };

    const url = `${process.env.KEYCLOAK_ISSUER}/protocol/openid-connect/token`;

    const response = await fetch(url, options);

    if (!response.ok) {
      const data = await response.json();
      console.error('error: ', data);
      return { error: 'RefreshAccessTokenError' };
    }

    const refreshedTokens: RefreshAccessTokenResponseFromKC = await response.json();

    return {
      ...refreshedTokens,
      accessTokenExpires: Math.floor(Date.now() / 1000) + refreshedTokens.expires_in,
    };
  } catch (error) {
    console.error(error);
    return {
      refresh_token,
      error: 'RefreshAccessTokenError',
    };
  }
};

/**
 * The function `getAccountMangementUrl` returns an object containing a URL and its corresponding parts
 * for managing a user account.
 * @returns The function `getAccountMangementUrl` returns an object of type `AccountMangement`. The
 * object contains two properties: `url` and `parts`. The `url` property is a string that represents
 * the constructed URL using the `queryString.stringifyUrl` function. The `parts` property is an object
 * that contains the individual parts used to construct the URL, including `baseUrl`, `referrer`,
 */
export const getAccountMangementUrl = () => {
  const url = queryString.stringifyUrl({
    url: `${process.env.KEYCLOAK_ISSUER}/account/`,
    query: {
      referrer: process.env.KEYCLOAK_ID,
      referrer_uri: `${process.env.NEXTAUTH_URL}/manage-account`,
    },
  });

  const response: AccountManagement = {
    url,
    parts: {
      baseUrl: `${process.env.KEYCLOAK_ISSUER}/account/`,
      referrer: process.env.KEYCLOAK_ID,
      referrer_uri: `${process.env.NEXTAUTH_URL}/manage-account`,
    },
  };
  return response;
};

/**
 * The function `getLinkedAccounts` retrieves linked accounts for a given user and obtains tokens for
 * each identity provider.
 * @param {string} access_token - The `access_token` parameter is a string that represents the access
 * token used for authentication and authorization. It is typically obtained after a user successfully
 * logs in or authenticates with a service.
 * @param {string} username - The `username` parameter is a string that represents the username of the
 * user for whom you want to retrieve the linked accounts.
 * @returns a Promise that resolves to an array of LinkedAccount objects.
 */
export const getLinkedAccounts = async (access_token: string, username: string) => {
  const response = await lincsAuthApi.users.getLinkedAccounts({
    headers: { authorization: `Bearer ${access_token}` },
    params: { username },
  });

  if (response.status === 401 || response.status === 404 || response.status === 500) {
    console.warn(response.body.message);
    return;
  }

  if (response.status !== 200) {
    console.warn({ error: 'something went wrong' });
    return;
  }

  //* Get tokens for each identity providers
  const linkedAccounts = response.body as LinkedAccount[];
  for (const linkedAccount of linkedAccounts) {
    const idpToken = await getIPTokens(access_token, linkedAccount.identityProvider);
    if (idpToken) linkedAccount.token = idpToken;
  }

  return linkedAccounts;
};

/**
 * The `getIPTokens` function retrieves an identity provider token using an access token and the
 * identity provider name.
 * @param {string} accessToken - The `accessToken` parameter is a string that represents the access
 * token used for authentication. It is typically obtained after a successful login or authentication process.
 * @param {string} identityProvider - The `identityProvider` parameter is a string that represents the
 * name of the identity provider. It is used to construct the URL for fetching the identity provider token.
 * @returns The function `getIPTokens` returns a Promise that resolves to an `IdentityProviderToken` object or `undefined`.
 */
const getIPTokens = async (
  accessToken: string,
  identityProvider: string,
): Promise<IdentityProviderToken | undefined> => {
  const response = await fetch(`${process.env.KEYCLOAK_ISSUER}/broker/${identityProvider}/token`, {
    method: 'GET',
    headers: { Authorization: `Bearer ${accessToken}` },
  });

  if (!response.ok) return;

  let idpToken: IdentityProviderToken;
  const data = await response.text(); // Parse it as text

  try {
    idpToken = JSON.parse(data); // Try to parse as JSON
  } catch (error) {
    const dataParsed = queryString.parse(data);
    idpToken = JSON.parse(JSON.stringify(dataParsed));
  }

  if (!idpToken) return;

  //* Refresh tokens if it is expired and only for gitlab
  //! Ideally we should refresh tokens from the backend
  //? But tehre is not a good soultiion for this at the moment: https://github.com/keycloak/keycloak/issues/14644
  //? Meanwhile, we refresh tokens in the frontend by relinking the idenity provider if the token is expired
  // * From: https://www.keycloak.org/docs/latest/server_development/#refreshing-external-tokens
  // If you are using the external token generated by logging into the provider (i.e. a Facebook or GitHub token)
  // you can refresh this token by re-initiating the account linking API.

  return idpToken;
};
