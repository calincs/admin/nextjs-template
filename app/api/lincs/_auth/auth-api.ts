import { contract } from '@lincs.project/auth-api-contract';
import { initClient } from '@ts-rest/core';

export const lincsAuthApi = initClient(contract.v1, {
  baseUrl: process.env.LINCS_AUTH_API_URL,
  baseHeaders: {},
});
