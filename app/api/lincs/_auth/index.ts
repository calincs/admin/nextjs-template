import type { Account, NextAuthOptions, Profile, User } from 'next-auth';
import { JWT } from 'next-auth/jwt';
import KeycloakProvider from 'next-auth/providers/keycloak';
import {
  getAccountMangementUrl,
  getLinkedAccounts,
  isAccessTokenExpired,
  refreshAccessToken,
} from './keycloak';

const SESSION_MAX_AGE = 30 * 60; //* 30 minutes. [default is 30 days -> 30 * 24 * 60 * 60];

const sessionReasons = ['accountUpdated', 'linkAccount', 'refreshIDPTokens'] as const;
type SessionReason = (typeof sessionReasons)[number];

interface Session {
  reason?: SessionReason;
}

/* The code block is defining the authentication options for NextAuth. */
export const authOptions: NextAuthOptions = {
  providers: [
    KeycloakProvider({
      clientId: process.env.KEYCLOAK_ID,
      clientSecret: process.env.KEYCLOAK_SECRET,
      issuer: process.env.KEYCLOAK_ISSUER,
    }),
  ],
  session: {
    maxAge: SESSION_MAX_AGE,
  },
  callbacks: {
    jwt: async ({ account, profile, session, token, trigger, user }) => {
      if (trigger === 'signIn') {
        token = await processTokenSignIn({ account, profile, token, user });
      }

      if (trigger !== 'signIn') {
        const isTokenExpired = await isAccessTokenExpired(token.accessTokenExpires);
        if (isTokenExpired) {
          const refreshedToken = await refreshAccessToken(token.refresh_token);
          token = { ...token, ...refreshedToken };
        }
      }

      const _session: Session | undefined = session;

      //Reasons to refresh user linkedAccounts
      if (_session?.reason && sessionReasons.includes(_session.reason)) {
        if (token?.access_token && token?.username) {
          const linkedAccounts = await getLinkedAccounts(token.access_token, token.username);
          if (linkedAccounts) token.linkedAccounts = linkedAccounts;
        }
      }

      return token;
    },
    session: async ({ session, token }) => {
      session = {
        ...session,
        access_token: token.access_token,
        accessTokenExpires: token.accessTokenExpires,
        provider: token.provider,
        error: token.error,
      };

      session.user = {
        ...session.user,
        accountManagement: token.accountMangement,
        identityProvider: token.identityProvider,
        image: token.picture,
        username: token.username,
      };

      //* linked accounts - only the essentials
      session.user.linkedAccounts = token.linkedAccounts?.map((account) => {
        const { token, ...rest } = account;
        return {
          ...rest,
          access_token: token?.access_token,
          accessTokenExpires: token?.accessTokenExpiration ?? undefined,
        };
      });

      return session;
    },
  },
};

interface ProcessTokenSignInParams {
  account?: Account | null;
  profile?: Profile;
  token: JWT;
  user: User;
}

/**
 * The function `processTokenSignIn` takes in parameters related to token, account, profile, and user,
 * and returns a modified token object with additional information.
 * @param {ProcessTokenSignInParams}  - - `token`: The JWT token that will be returned after processing.
 * @returns The function `processTokenSignIn` returns the `token` object after modifying it with
 * additional information such as provider info, profile attributes, user info, account management URL,
 * and linked accounts.
 */
const processTokenSignIn = async ({ token, account, profile, user }: ProcessTokenSignInParams) => {
  //* Incoporate provider info (access_token, refresh_token) into JWT
  if (account) {
    token = {
      ...token,
      access_token: account.access_token,
      accessTokenExpires: account.expires_at, //* this is the expire date, not the duration.
      provider: account.provider,
      refresh_token: account.refresh_token,
    };
  }

  //* Add pofile attributes
  if (profile) {
    token.identityProvider = profile?.identity_provider;
    token.username = profile?.preferred_username; //* preferredUsername is mapped to 'username' on Keycloak
  }

  //*  Incorporate user Info
  token = { ...token, user };

  //* Link to user account management
  token.accountMangement = getAccountMangementUrl();

  //* Check and Add Linked accounts
  if (token?.access_token && token?.username) {
    const linkedAccounts = await getLinkedAccounts(token.access_token, token.username);
    if (linkedAccounts) token.linkedAccounts = linkedAccounts;
  }

  return token;
};
