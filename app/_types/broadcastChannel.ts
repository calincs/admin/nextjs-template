export interface Message {
  success: boolean;
  error?: string;
  codeVerifier?: string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [x: string]: any;
}

export interface LinkAccountMessage extends Message {
  isRefresh?: boolean;
  provider?: string;
}
