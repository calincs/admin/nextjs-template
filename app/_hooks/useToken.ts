import { SwrFetcher } from '@/app/_util';
import { JWT } from 'next-auth/jwt';
import useSWR from 'swr';

/**
 * The `useToken` function is a custom hook that fetches a JWT token from the server and returns the
 * token data, error status, and loading status.
 * @returns The `useToken` function returns an object with the following properties:
 */
export const useToken = () => {
  const { data, error, isLoading } = useSWR<JWT>('api/auth/jwt', SwrFetcher);

  return {
    data,
    isError: error,
    isLoading,
  };
};
