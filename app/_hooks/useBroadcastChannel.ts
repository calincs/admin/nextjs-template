'use client';

import { BroadcastChannel } from 'broadcast-channel';
import { secondsToMilliseconds } from 'date-fns';

const _TIME_OUT = secondsToMilliseconds(30); //MAX TIME to callback the origin

interface Options {
  timeout?: number;
}

/**
 * The `useBroadcastChannel` function provides a utility for opening a new window, creating a broadcast
 * channel, and returning a promise that resolves with the response received on the broadcast channel
 * or rejects with a timeout error.
 * @returns The `useBroadcastChannel` function returns an object with a single property
 * `openAuxWindowChannel`.
 */
export const useBroadcastChannel = () => {
  /**
   * The `openAuxWindowChannel` function opens a new window, creates a broadcast channel, and returns a promise that
   * resolves with the response received on the broadcast channel or rejects with a timeout error.
   * @param {string} channel - The `channel` parameter is a string that represents the name of the
   * broadcast channel. It is used to establish communication between the main window and the auxiliary
   * window. Both windows should use the same channel name to communicate with each other.
   * @param {string} url - The `url` parameter is a string that represents the URL of the auxiliary
   * window that you want to open.
   * @param {Options} options - The `options` parameter is an object that contains additional
   * configuration options for the function. It has a default value of `{ timeout: _TIME_OUT }`, where
   * `_TIME_OUT` is a variable that holds the default timeout value (in milliseconds).
   * @returns The function `openAuxWindowChannel` returns a Promise that resolves to a value of type `T`.
   */
  const openAuxWindowChannel = <T = unknown>(
    channel: string,
    url: string,
    options: Options = { timeout: _TIME_OUT },
  ): Promise<T> => {
    const { timeout } = options;

    const broadcastChannel = new BroadcastChannel<T>(channel);

    window.open(url);

    return new Promise((resolve, reject) => {
      const timer = setTimeout(() => {
        broadcastChannel.close();
        clearTimeout(timer);
        reject('timeout');
      }, timeout);

      broadcastChannel.onmessage = async (response: T) => {
        broadcastChannel.close();
        clearTimeout(timer);
        return resolve(response);
      };
    });
  };

  return {
    openAuxWindowChannel,
  };
};
