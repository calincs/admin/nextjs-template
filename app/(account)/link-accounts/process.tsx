'use client';

import type { SiteBroadcastChannel } from '@/app//_types';
import styles from '@/app/page.module.css';
import { BroadcastChannel } from 'broadcast-channel';
import 'client-only';
import { useEffect } from 'react';

interface Props {
  codeVerifier?: string;
  error?: string;
  isRefresh?: boolean;
  provider?: string;
}

export const Process = (props: Props) => {
  useEffect(() => {
    const channel = new BroadcastChannel('keycloak-link-account');
    const success = !props.error;
    const message: SiteBroadcastChannel.LinkAccountMessage = { success, ...props };
    channel.postMessage(message);
    window.close();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleClick = () => {
    window.close();
  };

  return (
    <div>
      <button onClick={handleClick} className={styles.button}>
        Close this window
      </button>
    </div>
  );
};
