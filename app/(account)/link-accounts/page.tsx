import styles from '@/app/page.module.css';
import { Process } from './process';

export default async function LinkAccounts({
  searchParams,
}: {
  searchParams: Record<string, string | string[] | undefined>;
}) {
  const codeVerifier = Array.isArray(searchParams.codeVerifier)
    ? searchParams.codeVerifier[0]
    : searchParams.codeVerifier;
  const error = Array.isArray(searchParams.error) ? searchParams.error[0] : searchParams.error;

  const provider = Array.isArray(searchParams.provider)
    ? searchParams.provider[0]
    : searchParams.provider;

  const isRefresh = Array.isArray(searchParams.isRefresh)
    ? searchParams.isRefresh[0]
    : searchParams.isRefresh;

  return (
    <main className={styles.main}>
      {provider && (
        <>
          <h2 style={{ marginBottom: '1rem' }}>
            {isRefresh ? 'Refresh Tokens' : 'Linked Account'}
          </h2>
          <p>{isRefresh ? <>{provider} tokens refreshed</> : <>{provider} linked</>}</p>
        </>
      )}
      {error && <p>{error}</p>}
      <div className={styles.center}>
        <Process
          {...{ codeVerifier, error, isRefresh: isRefresh === 'true' ? true : false, provider }}
        />
      </div>
    </main>
  );
}
