import styles from '@/app/page.module.css';
import { Process } from './process';

export default async function LinkAccounts({
  searchParams,
}: {
  searchParams: Record<string, string | string[] | undefined>;
}) {
  const codeVerifier = Array.isArray(searchParams.codeVerifier)
    ? searchParams.codeVerifier[0]
    : searchParams.codeVerifier;
  const error = Array.isArray(searchParams.error) ? searchParams.error[0] : searchParams.error;

  return (
    <main className={styles.main}>
      <h2 style={{ marginBottom: '1rem' }}>Account Manage callback</h2>
      {error && <p>{error}</p>}
      <div className={styles.center}>
        <Process {...{ codeVerifier, error }} />
      </div>
    </main>
  );
}
