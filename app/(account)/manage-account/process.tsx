'use client';

import type { SiteBroadcastChannel } from '@/app/_types';
import styles from '@/app/page.module.css';
import { BroadcastChannel } from 'broadcast-channel';
import 'client-only';
import { useEffect } from 'react';

interface Props {
  codeVerifier?: string;
  error?: string;
}

export const Process = ({ codeVerifier, error }: Props) => {
  useEffect(() => {
    const channel = new BroadcastChannel('keycloak-account-manager');
    const success = !error;
    const message: SiteBroadcastChannel.Message = { success, codeVerifier, error };
    channel.postMessage(message);
    window.close();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleClick = () => {
    window.close();
  };

  return (
    <div>
      <button onClick={handleClick} className={styles.button}>
        Close this window
      </button>
    </div>
  );
};
