import Link from 'next/link';
import styles from './page.module.css';

export default function NotFound() {
  return (
    <main className={styles.main}>
      <div className={styles.center}>
        <div className={styles.notFound}>
          <h2 className={styles.title}>Not Found</h2>
          <p>Could not find requested resource</p>
          <p>
            Go back{' '}
            <Link className={styles.link} href="/">
              Home
            </Link>
          </p>
        </div>
      </div>
    </main>
  );
}
