import Image from 'next/image';
import Link from 'next/link';
import styles from './header.module.css';

export function Header() {
  return (
    <header className={styles.header}>
      <div className={styles.logos}>
        <Link href="/">
          <picture>
            <source srcSet="/logo/lincs_dark.png" media="(prefers-color-scheme:dark)" />
            <Image src="/logo/lincs.png" width={75} height={30} alt="LINCS" />
          </picture>
        </Link>

        <hr className={styles.separator} />
        <Link href="/">
          <picture>
            <source srcSet="/logo/leaf_dark.png" media="(prefers-color-scheme:dark)" />
            <Image src="/logo/leaf.png" width={75} height={30} alt="LEAF" />
          </picture>
        </Link>
      </div>
    </header>
  );
}
