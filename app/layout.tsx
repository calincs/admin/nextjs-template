import { Inter } from 'next/font/google';
import { Header } from './_components';
import './globals.css';
import styles from './page.module.css';

const inter = Inter({ subsets: ['latin'] });

export const metadata = {
  title: 'LINCS | LEAF - Authentication Template',
  description: 'LINCS | LEAF - Authentication Template',
};

export default function RootLayout({ children }: { children: React.ReactNode }) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <div className={styles.page}>
          <Header />
          {children}
        </div>
      </body>
    </html>
  );
}
