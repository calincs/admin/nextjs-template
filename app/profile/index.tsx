'use client';

import { useSession } from 'next-auth/react';
import { SnackbarProvider } from 'notistack';
import { DataSection, IdentityProviders, User } from './components';
import { Bar } from './components/bar';
import styles from './profile.module.css';

export function Profile() {
  const { data: session } = useSession();
  const isAuthenticated = !!session;

  return (
    <div className={styles.main}>
      <SnackbarProvider>
        <Bar>
          <User />
        </Bar>
        {isAuthenticated && (
          <>
            <IdentityProviders />
            <DataSection />
          </>
        )}
      </SnackbarProvider>
    </div>
  );
}
