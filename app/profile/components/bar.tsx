'use client';

import classNames from 'classnames';
import classNamesCx from 'classnames/bind';
import { signIn, signOut, useSession } from 'next-auth/react';
import { MouseEvent, PropsWithChildren } from 'react';
import styles from '../profile.module.css';

export function Bar({ children }: PropsWithChildren) {
  const { data: session, status } = useSession();
  const isLoading = status === 'loading';
  const isAuthenticated = status === 'authenticated';

  const cx = classNamesCx.bind(styles);

  const handleClick = (event: MouseEvent<HTMLButtonElement, globalThis.MouseEvent>) => {
    event.preventDefault();
    if (isAuthenticated) return void signOut();

    void signIn('keycloak', undefined, { prompt: 'login' });
  };

  return (
    <>
      {isLoading && (
        <div className={styles.spinerContainer}>
          <span className={styles.spinner} />
        </div>
      )}
      <div
        className={cx(styles.bar, {
          loading: isLoading,
          unauthenticated: !isAuthenticated,
        })}
      >
        {children}
        <button
          className={classNames('button', cx(styles.button, { primary: !session }))}
          onClick={handleClick}
        >
          {!!session ? 'Sign out' : 'Sign In'}
        </button>
      </div>
    </>
  );
}
