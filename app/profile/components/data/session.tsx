'use client';

import { useSession } from 'next-auth/react';
import styles from './data.module.css';

export const SessionData = () => {
  const { data: session } = useSession();

  return (
    <div className={styles.container}>
      <small>Available on the frontend</small>
      <div className={styles.data}>
        <pre>{JSON.stringify(session, null, 2)}</pre>
      </div>
    </div>
  );
};
