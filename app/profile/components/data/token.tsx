'use client';

import { useToken } from '@/app/_hooks/useToken';
import styles from './data.module.css';

export const TokenData = () => {
  const { data } = useToken();

  return (
    <div className={styles.container}>
      <small>Available on the backend (must be fetched to use in the frontend).</small>
      <div className={styles.data}>{data && <pre>{JSON.stringify(data, null, 2)}</pre>}</div>
    </div>
  );
};
