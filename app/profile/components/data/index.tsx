'use client';

import classNames from 'classnames';
import classNamesCx from 'classnames/bind';
import { useSession } from 'next-auth/react';
import { useState } from 'react';
import styles from './data.module.css';
import { SessionData } from './session';
import { TokenData } from './token';

type Data = 'session' | 'token';

export const DataSection = () => {
  const { status } = useSession();
  const [data, setData] = useState<Data>('session');

  const cx = classNamesCx.bind(styles);

  const handleChangeDataShow = (value: Data) => setData(value);

  return (
    <div className={cx(styles.main, { loading: status === 'loading' })}>
      <div className={styles.switcherGroup}>
        <button
          className={classNames('button', cx(styles.button, { active: data === 'session' }))}
          onClick={() => handleChangeDataShow('session')}
        >
          Session
        </button>
        <button
          className={classNames('button', cx(styles.button, { active: data === 'token' }))}
          onClick={() => handleChangeDataShow('token')}
        >
          Token
        </button>
      </div>
      {data === 'session' ? <SessionData /> : <TokenData />}
    </div>
  );
};
