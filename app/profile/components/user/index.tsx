'use client';

import classNames from 'classnames';
import classNamesCx from 'classnames/bind';
import { useSession } from 'next-auth/react';
import { useState } from 'react';
import { HiExternalLink } from 'react-icons/hi';
import { IoMdRefresh } from 'react-icons/io';
import { useUser } from './useUser';
import styles from './user.module.css';

export const User = () => {
  const { data: session } = useSession();
  const { manageAccount, refreshAccount } = useUser();

  const [refreshing, setRefreshing] = useState(false);

  const cx = classNamesCx.bind(styles);

  const user = session?.user;

  const handleManageAccountClick = () => manageAccount();
  const handleManageRefreshClick = async () => {
    setRefreshing(true);
    await refreshAccount();
    setRefreshing(false);
  };

  if (!user) return <span>You are not signed in</span>;

  return (
    <div className={styles.container}>
      {user.image && (
        <span className={styles.avatar} style={{ backgroundImage: `url('${user.image}')` }} />
      )}
      <div className={styles.username}>
        <small>Signed in as</small>
        <div className={styles.components}>
          <span>
            <strong>{user.name ?? user.email}</strong>
            {user.username && <small> ({user.username})</small>}
          </span>
          <button
            className={classNames(
              'button',
              'iconButton',
              cx(styles.iconButton, { spinner: refreshing }),
            )}
            onClick={handleManageRefreshClick}
          >
            <IoMdRefresh />
          </button>
        </div>
        {!!user.accountManagement && (
          <button
            className={classNames('button', cx(styles.button))}
            onClick={handleManageAccountClick}
            style={{ marginLeft: '-6px' }}
          >
            <span>Manage Account</span>
            <HiExternalLink />
          </button>
        )}
      </div>
    </div>
  );
};
