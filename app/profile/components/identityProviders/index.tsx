'use client';

import { useLoadIdentityProviders } from '@/app/_hooks/useLoadIdentityProviders';
import classNames from 'classnames/bind';
import { useSession } from 'next-auth/react';
import { memo } from 'react';
import stylesProfile from '../../profile.module.css';
import styles from './identityProviders.module.css';
import { ProviderButton } from './providerButton';

export const IdentityProviders = memo(function IdentityProviders() {
  const { status } = useSession();
  const { providers, isLoading, isError } = useLoadIdentityProviders();
  const cx = classNames.bind(styles);
  return (
    <>
      {isError ? null : (
        <div className={cx(styles.bar, { loading: status === 'loading' })}>
          <p style={{ fontSize: '.9rem' }}>Linked Accounts</p>
          <div className={cx(styles.providers, { loading: isLoading })}>
            {isLoading ? (
              <span className={stylesProfile.spinner} />
            ) : providers ? (
              providers.map(({ enabled = false, providerId }) =>
                providerId ? (
                  <ProviderButton key={providerId} {...{ enabled, providerId }} />
                ) : (
                  <></>
                ),
              )
            ) : null}
          </div>
        </div>
      )}
    </>
  );
});
