'use client';

import classNames from 'classnames';
import classNamesCx from 'classnames/bind';
import { useSession } from 'next-auth/react';
import { useEffect, useState } from 'react';
import { BiRefresh } from 'react-icons/bi';
import { BsCheck2Circle } from 'react-icons/bs';
import { MdRadioButtonUnchecked } from 'react-icons/md';
import styles from './identityProviders.module.css';
import { useProviders } from './useProviders';

interface Props {
  enabled: boolean;
  providerId: string;
}

export const ProviderButton = ({ enabled, providerId }: Props) => {
  const { isProviderLinked, isTokenExpired, linkProvider, refreshToken } = useProviders(providerId);
  const { data: session } = useSession();

  const [refreshing, setRefreshing] = useState(false);
  const [tokenExpired, setTokenExpired] = useState(false);

  const isLinked = isProviderLinked(providerId);

  const cx = classNamesCx.bind(styles);

  useEffect(() => {
    checkTokens();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [session]);

  const checkTokens = async () => {
    const isExpired = await isTokenExpired();
    setTokenExpired(isExpired);
  };

  const handleClick = async () => {
    if (!isLinked || tokenExpired) setRefreshing(true);

    if (isLinked && tokenExpired) await refreshToken();
    if (!isLinked) await linkProvider();

    return setRefreshing(false);
  };

  return (
    <button
      key={providerId}
      className={classNames(
        'button',
        styles.button,
        cx({
          disabled: !enabled,
          linked: isLinked && !tokenExpired,
          spinner: refreshing,
        }),
      )}
      disabled={!enabled}
      onClick={handleClick}
      style={{ textTransform: 'capitalize' }}
    >
      {tokenExpired ? <BiRefresh /> : isLinked ? <BsCheck2Circle /> : <MdRadioButtonUnchecked />}
      {providerId}
    </button>
  );
};
