# Next.js LINCS template

This is a [Next.js](https://nextjs.org/) project with [NextAuth.js](https://next-auth.js.org/) integration. It has Keycloak authentication built in.

It serves as a template for LINCS Project Commons Applications.

## Getting Started

### Make a copy of this repo

Copy the files instead of forking or cloning them to avoid inheriting git history.

### Environment Variables

This project requires a few environment variables to work. Copy the `.env.local.example` file in this directory to `.env.local` (which will be ignored by Git). Make sure to edit `.env.local` and set the following variables (check the comments in the file for more details):

```text
NEXTAUTH_URL=http://localhost:3000 #
NEXTAUTH_SECRET=<generate with openssl rand -base64 32>
KEYCLOAK_ID=<name of the Keycloak client>
KEYCLOAK_SECRET=<the Keycloak client secret>
KEYCLOAK_ISSUER=https://keycloak.dev.lincsproject.ca/realms/lincs
LINCS_AUTH_API_URL=https://auth-api.dev.lincsproject.ca
```

### Configure Authentication Providers

This template uses [NextAuth.js](https://next-auth.js.org/) to orchestrate Authentication via Lincs Keycloak. It handles sign-in and sign-out, access_token refresh (keycloak), and a few extra functionalities, such as listing supported external identity providers, linking the user account to these providers, checking users’ linked accounts, and refreshing external identity providers’ access_tokens.

Review and update options in Lincs [`auth.ts`](./app/api/lincs/_auth/index.ts) and in the files in this [folder](./app/api/lincs/_auth) as needed.

Note: While this template was made exclusively for LINCS Project, it is possible to replace the authentication route altogether and use other providers (See [NextAuth.js](https://next-auth.js.org/) for more details).

### Start Editing

Every project is different. The best place to start is to edit the [main page](./app/page.tsx), save the changes and check the results. However, since this template uses Next.js, you may need to understand the core concept of both [React](https://react.dev/), [Next.js](https://nextjs.org/), and [Typescript](https://www.typescriptlang.org/).

## Use this template in conjunction with other projects

This template can serve as a base for other projects that are more flexible and portable. Combining this template and other applications in a monorepo allows us to deploy the project as a standalone (LiNCS Commons) and as an independent package to be published on NPM. See how we implement this solution on [NERVE](https://gitlab.com/calincs/conversion/NERVE).

## Run

Run the development server:

```bash
npm install
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

### Run with Docker

```bash
docker compose up
```

## Deploy

### Linter

**Attention!**

This project uses and deploys ESM. We have configured a tight linter with strict Typescript and format (prettier) rules. Some of these rules can be relaxed if needed. Check eslint config: `.eslintrc.cjs`.

The linter always runs before any commit. But you can also run it at any time to check the code.

```node
npm run lint
```

### Build

To build and check Typescript type safety:

```bash
npm run build
```

## Development documentation

Latest NextJS documentation: <https://nextjs.org/docs>

If you want to look at a UI framework:

- [MUI](https://mui.com/material-ui/)
- [NextUI v2](https://nextui-docs-v2.vercel.app/)
- [PrimeReact](https://primereact.org/)
- [Mantine UI](https://ui.mantine.dev/)
- [React Icons](https://react-icons.github.io/react-icons/)
