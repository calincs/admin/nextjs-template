module.exports = {
  path: 'git-cz',
  format: '{type}: {emoji}{subject}',
  maxMessageLength: 110,
  questions: ['type', 'subject', 'body', 'breaking', 'issues'],
};
